package com.chanatda.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank worawit = new BookBank("Worawit", 100);
        worawit.print();
        worawit.deposit(50);
        worawit.print();
        worawit.withdraw(50);
        worawit.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweed = new BookBank( "Praweed",10);
        praweed.deposit(10000000);
        praweed.withdraw(1000000);
        praweed.print();

    }
}
