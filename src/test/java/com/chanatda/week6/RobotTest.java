package com.chanatda.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());

    }

    @Test
    public void shouldRobotUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());

    }

    @Test
    public void shouldRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(9);
        assertEquals(true, result);
        assertEquals(18, robot.getY());
    }

    @Test
    public void shouldRobotDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 5);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotDownNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(11);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRobotDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX - 1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldCreateRobotleftSuccess() {
        Robot robot = new Robot("Robot", 'R', 12, 11);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(11, robot.getX());

    }

    @Test
    public void shouldCreateRobotleftNFail1() {
        Robot robot = new Robot("Robot", 'R', 12, 11);
        boolean result = robot.left(24);
        assertEquals(false, result);
        assertEquals(0, robot.getX());

    }

    @Test
    public void shouldCreateRobotleftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 12, 11);
        boolean result = robot.left(12);
        assertEquals(true, result);
        assertEquals(0, robot.getX());

    }

    @Test
    public void shouldCreateRobotleftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 12, 11);
        boolean result = robot.left(6);
        assertEquals(true, result);
        assertEquals(6, robot.getX());

    }

    @Test
    public void shouldCreateRobotrightSuccess() {
        Robot robot = new Robot("Robot", 'R', 3, 11);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(4, robot.getX());

    }

    @Test
    public void shouldCreateRobotrightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 3, 11);
        boolean result = robot.right(3);
        assertEquals(true, result);
        assertEquals(6, robot.getX());

    }

    @Test
    public void shouldCreateRobotrightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 11, 11);
        boolean result = robot.right(2);
        assertEquals(true, result);
        assertEquals(13, robot.getX());

    }

    @Test
    public void shouldCreateRobotrightNfail1() {
        Robot robot = new Robot("Robot", 'R', 11, 11);
        boolean result = robot.right(11);
        assertEquals(false, result);
        assertEquals(19, robot.getX());

    }

    @Test
    public void shouldRobotleftFailAtMin() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 5);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }

    @Test
    public void shouldRobotrightFailAtMax() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 5);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }
}
