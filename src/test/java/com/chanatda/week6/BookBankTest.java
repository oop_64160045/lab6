package com.chanatda.week6;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {

    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(), 0.00001);

    }

    @Test
    public void shouldDepositNagative() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(), 0.00001);

    }

    @Test
    public void shouldWithDdrawSuccess() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(), 0.00001);

    }

    @Test
    public void shouldDepositdrawNagative() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(), 0.00001);

    }
    @Test
    public void shouldDepositdrawBalance() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(), 0.00001);

    }
    @Test
    public void shouldDepositdraw100Balance100() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getBalance(), 0.00001);

    }
    @Test
    public void shouldDepositwithdraw() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(1000);
        boolean result = book.withdraw(500);
        assertEquals(true, result);
        assertEquals(500, book.getBalance(), 0.00001);

    }
}
